package sv.iuh.fit.episode.controllers;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import sv.iuh.fit.episode.Models.Episode;
import sv.iuh.fit.episode.Models.Movie;
import sv.iuh.fit.episode.repositories.DirectorRepository;
import sv.iuh.fit.episode.repositories.EpisodeRepository;
import sv.iuh.fit.episode.repositories.MovieRepository;

import java.util.List;

@RestController
//@RequestMapping("/api/episodes")
@RequestMapping("/api")
//@AllArgsConstructor
public class EpisodeController {
    @Autowired
    private EpisodeRepository episodeRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private DirectorRepository directorRepository;

    @Value("${movie.service.url}")
    private String directorUrl;

    @GetMapping("/episodes")
    @Cacheable(value = "episodes")
    public List<Episode> getAllEpisode() {

        return episodeRepository.findAll();
    }

    @GetMapping("/episode/{episodeId}")
    @Cacheable(value = "episode", key = "#episodeId")
    public Episode getEpisodeById(@PathVariable Long episodeId) {
        return episodeRepository.findById(episodeId).orElse(null);
    }

//    @GetMapping("/episodes/{movieId}")
//    @Cacheable(value = "episodes")
//    public List<Episode> getEpisodeByMovieId(@PathVariable Long movieId) {
//        return episodeRepository.getAllByMovieId(movieId);
//    }

    //    @PostMapping("/createEpisode")
//    public Episode addEpisode(@RequestBody Episode episode) {return episodeRepository.save(episode);}
    @RateLimiter(name = "rateLimiterApi")
//    @CircuitBreaker(name = "circuitBreakerApi", fallbackMethod = "fallbackMethod")
    @PostMapping("episodes/addEpisodeByMovieId/{movieId}")
//    @CachePut(value = "episodes", key = "#movieId")
    public Episode addEpisode(@RequestBody Episode episode, @PathVariable String movieId) {
        RestTemplate restTemplate = new RestTemplate();
        Movie movie = restTemplate.getForObject(directorUrl + movieId, Movie.class);
        System.out.println(2);
        System.out.println(movie);
        if (movie == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Movie not found");
        } else {
            System.out.println(movie.getDirector());
            directorRepository.save(movie.getDirector());
            System.out.println(movie);
            movieRepository.save(movie);
            Episode newEpisode = new Episode(episode.getNameEpisode(), episode.getEpisodeNo(), episode.getReleaseDate(), episode.getTimeDuration(), movie);

            return episodeRepository.save(newEpisode);
        }
    }

//    public Episode fallbackMethod(RuntimeException runtimeException) {
//        System.out.println("Cannot retrieve movies. Executing fallback logic.");
//        return new Episode();
//    }

    @PutMapping("episodes/updateEpisode/{episodeId}")
    @CachePut(value = "episode", key = "#episodeId")
    public Episode updateEpisodeByEpisodeId(@RequestBody Episode episode, @PathVariable Long episodeId) {
        Episode checkExist = episodeRepository.findById(episodeId).orElse(null);
        if (checkExist == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Episode not found");
        } else {
            checkExist.setNameEpisode(episode.getNameEpisode());
            checkExist.setEpisodeNo(episode.getEpisodeNo());
            checkExist.setReleaseDate(episode.getReleaseDate());
            checkExist.setTimeDuration(episode.getTimeDuration());
            checkExist.getMovie();
            return episodeRepository.save(checkExist);
        }
    }

    @PostMapping("episodes/deleteEpisodeById/{episodeId}")
    @CacheEvict(value = "episode", key = "#episodeId")
    public ResponseEntity<String> deleteEpisode(@PathVariable("episodeId") Long episodeId) {
        Episode episode = episodeRepository.findById(episodeId).orElse(null);
        if (episode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Episode with id " + episodeId + " not found");
        } else {
            episodeRepository.deleteById(episodeId);
            return ResponseEntity.ok("Delete success with episode id: " + episodeId);
        }
    }
}

