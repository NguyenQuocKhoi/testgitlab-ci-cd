package sv.iuh.fit.episode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class EpisodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpisodeApplication.class, args);
	}

}
