package sv.iuh.fit.user.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sv.iuh.fit.user.models.User;
import sv.iuh.fit.user.repositories.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @PostMapping("/createUser")
    User CreateUser(@RequestBody User user) {
        return userRepository.save(user);

    }

    @GetMapping("/")
    List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/{userId}")
    User GetUser(@PathVariable Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    @PutMapping("/updateUser/{userId}")
    ResponseEntity<User> updateUser(@PathVariable long userId, @RequestBody User user) {
        User checkExist = userRepository.findById(userId).orElse(null);
        if (checkExist != null) {
            checkExist.setFirstName(user.getFirstName());
            checkExist.setLastName(user.getLastName());
            checkExist.setUsername(user.getUsername());
            checkExist.setGender(user.getGender());
            checkExist.setDob(user.getDob());
            userRepository.save(checkExist);
            return ResponseEntity.ok(checkExist);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }


    @PostMapping("/deleteUser/{userId}")
    public ResponseEntity<String> deleteDirector(@PathVariable("userId") Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with id " + userId + " not found");
        } else {
            userRepository.deleteById(userId);
            return ResponseEntity.ok("Delete success with user id: " + userId);
        }
    }
}
