package sv.iuh.fit.user.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sv.iuh.fit.user.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
