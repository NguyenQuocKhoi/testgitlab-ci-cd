package sv.iuh.fit.movie.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sv.iuh.fit.movie.enums.MovieGenre;
import sv.iuh.fit.movie.enums.MovieType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String movieName;
    @Enumerated(EnumType.ORDINAL)
    private MovieType movieType; //phim le phim bo ...
    private String uri;
    private String rating;
    private String description;
    @Enumerated(EnumType.ORDINAL)
    private MovieGenre movieGenre;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate releaseDate;
    @ManyToOne
    private Director director;


    public Movie(String movieName, MovieType movieType, String uri, String rating, String description, MovieGenre movieGenre, LocalDate releaseDate, Director director) {
        this.movieName = movieName;
        this.movieType = movieType;
        this.uri = uri;
        this.rating = rating;
        this.description = description;
        this.movieGenre = movieGenre;
        this.releaseDate = releaseDate;
        this.director = director;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
