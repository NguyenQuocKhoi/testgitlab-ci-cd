package sv.iuh.fit.movie.controllers;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import sv.iuh.fit.movie.repositories.DirectorRepository;
import sv.iuh.fit.movie.repositories.MovieRepository;
import sv.iuh.fit.movie.models.Director;
import sv.iuh.fit.movie.models.Movie;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/movies")
//@AllArgsConstructor
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private DirectorRepository directorRepository;
    @Autowired
    private RedisTemplate redisTemplate;
    //    private final MovieDao movieDao;
    @Value("${director.service.url}")
    private String directorServiceUrl;

    @GetMapping("/")
    public Set<Movie> getAllMovies() {

        Set<Movie> list = redisTemplate.opsForSet().members("movies");
        return list;
    }

    @GetMapping("/getAll")
    public List<Movie> getAll() {
        List<Movie> list = movieRepository.findAll();
        return list;
    }

    @GetMapping("/{directorId}")
//    @Retry(name = "retry1")
    List<Movie> getMovieByDirectorId(@PathVariable Long directorId) {
        return movieRepository.getAllByDirectorId(directorId);

    }

    @PostMapping("/addMoviesByDirectorId/{directorId}")
    @Retry(name = "retry1")
    @RateLimiter(name = "rateLimiterApi")
    public Movie addMovie(@RequestBody Movie movie, @PathVariable String directorId) {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = directorServiceUrl + directorId;
        Director director = restTemplate.getForObject(apiUrl, Director.class);

        System.out.println(director);
        if (director == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Director not found");
        } else {
            directorRepository.save(director);
            Movie newMovie = new Movie(movie.getMovieName(), movie.getMovieType(), movie.getUri(), movie.getRating(), movie.getDescription(), movie.getMovieGenre()
                    , movie.getReleaseDate(), director);
            System.out.println(newMovie);

            Movie addmovie = movieRepository.save(newMovie);
            redisTemplate.opsForSet().add("movies", addmovie);
            return addmovie;
        }
    }

    @PostMapping("/createMovie")
    public Movie addMovie1(@RequestBody Movie movie) {
        return movieRepository.save(movie);
    }

    @PutMapping("/updateMovieByMovieId/{movieId}")
    public Movie updateMovieByMovieId(@RequestBody Movie movie, @PathVariable Long movieId) {
        Movie checkExist = movieRepository.findById(movieId).orElse(null);
        if (checkExist == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie not found");
        } else {
            checkExist.setMovieName(movie.getMovieName());
            checkExist.setMovieType(movie.getMovieType());
            checkExist.setUri(movie.getUri());
            checkExist.setRating(movie.getRating());
            checkExist.setDescription(movie.getDescription());
            checkExist.setMovieGenre(movie.getMovieGenre());
            checkExist.setReleaseDate(movie.getReleaseDate());
            checkExist.getDirector();
            Movie update = movieRepository.save(checkExist);
            redisTemplate.opsForSet().remove("movies", movie.getId());
            redisTemplate.opsForSet().add("movies", update);
            return update;

        }
    }

    @PostMapping("/deleteMovieById/{movieId}")
    public ResponseEntity<String> deleteMovie(@PathVariable("movieId") Long movieId) {
        Movie movie = movieRepository.findById(movieId).orElse(null);
        if (movie == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Movie with id " + movieId + " not found");
        } else {
            movieRepository.deleteById(movieId);
            return ResponseEntity.ok("Delete success with movie id: " + movieId);
        }
    }


    @GetMapping("/findMovieByDirector/{directorName}")
    ResponseEntity findMovieByDirector(@PathVariable String directorName) {
        Set<Movie> list = redisTemplate.opsForSet().members("movies");
        if (list == null || list.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Set<Movie> result = new HashSet<>();
        for (Movie movie : list) {
            Director director = movie.getDirector();
            if (director != null &&
                    (director.getLastName().toLowerCase() + " " + director.getFirstName().toLowerCase())
                            .contains(directorName.toLowerCase())) {
                result.add(movie);
            }
        }
        if (result.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/findMovieByName/{movieName}")
    ResponseEntity findMovieByName(@PathVariable String movieName) {
        Set<Movie> list = redisTemplate.opsForSet().members("movies");
        Set<Movie> result = new HashSet<>();
        for (Movie movie : list) {
            if (movie.getMovieName().toLowerCase().contains(movieName.toLowerCase())) {
                result.add(movie);
            }
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/findMovieByMovieId/{movieId}")
    public Movie findMovieById(@PathVariable long movieId) {
//        throw new RuntimeException("Failed to fetch movie by id");
        return movieRepository.findById(movieId).orElse(null);
    }


}
