package sv.iuh.fit.comment.CommentControllers;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import sv.iuh.fit.comment.Repositories.CommentRepository;
import sv.iuh.fit.comment.Repositories.MovieRepository;
import sv.iuh.fit.comment.Repositories.UserRepository;
import sv.iuh.fit.comment.models.Comment;
import sv.iuh.fit.comment.models.Movie;
import sv.iuh.fit.comment.models.User;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
//@AllArgsConstructor
public class CommentController {
    @Autowired
    private  CommentRepository commentRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private UserRepository userRepository;

    @Value("${user.service.url}")
    private String userServeUrl;

    @GetMapping("/")
    public List<Comment> getAllComments(){return commentRepository.findAll();}

    @GetMapping("/findCommentByMovieId/{movieId}")
    @Retry(name = "retry1")
    List<Comment> getCommentByMovieId(@PathVariable Long movieId){return commentRepository.getAllByMovieId(movieId);}

    @GetMapping("/findCommentByUserId/{userId}")
    @Retry(name = "retry2")
    List<Comment> getCommentByUserId(@PathVariable Long userId){return commentRepository.getAllByMovieId(userId);}

    @PostMapping("/addCommentForMovieByUserId/{userId}")
    public Comment addComment(@RequestBody Comment comment, @PathVariable String userId){
        RestTemplate restTemplate = new RestTemplate();
        User user = restTemplate.getForObject(userServeUrl + userId, User.class);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User not found");
        }
        else {
            System.out.println(user);
            userRepository.save(user);
            Comment newComment = new Comment(comment.getContent(), comment.getTime(), comment.getMovie(), user);
            return commentRepository.save(newComment);
        }
    }
    @DeleteMapping("/deleteCommentByUserId/{userId}")
    public ResponseEntity<String> deleteCommentByUserId(@PathVariable Long userId) {
        List<Comment> comments = commentRepository.getAllByUserId(userId);

        if (comments.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No comments found for user with ID: " + userId);
        }

        commentRepository.deleteAll(comments);
        return ResponseEntity.ok("Comments deleted successfully for user with ID: " + userId);
    }

}
