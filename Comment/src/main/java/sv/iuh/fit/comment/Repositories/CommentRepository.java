package sv.iuh.fit.comment.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sv.iuh.fit.comment.models.Comment;

import java.util.List;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> getAllByMovieId(long id);
    List<Comment> getAllByUserId(long id);
}
