package sv.iuh.fit.comment.enums;

import java.io.Serializable;

public enum MovieGenre implements Serializable {
    PHIM_CO_TRANG(1),
    PHIM_TAM_LY(2),
    PHIM_VO_THUAT(3),
    PHIM_PHIEU_LUU(4),
    PHIM_HINH_SU(5),
    PHIM_VIEN_TUONG(6),
    PHIM_THAN_THOAI(7),
    TV_SHOWS(8),
    PHIM_KICH_TINH(9),
    PHIM_HAI(10),
    PHIM_HANH_DONG(11),
    PHIM_CHIEN_TRANH(12),
    PHIM_TINH_CAM(13),
    PHIM_KINH_DI(14),
    PHIM_AM_NHAC(15),
    PHIM_GIA_DINH(16),
    PHIM_HOAT_HINH(17),
    PHIM_KHOA_HOC(18),
    ;
    private final int value;

    MovieGenre(int value) {
        this.value = value;
    }
}
