package sv.iuh.fit.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import sv.iuh.fit.comment.Repositories.CommentRepository;
import sv.iuh.fit.comment.Repositories.UserRepository;
import sv.iuh.fit.comment.models.Comment;
import sv.iuh.fit.comment.models.Movie;
import sv.iuh.fit.comment.models.User;

import java.time.LocalDate;
import java.util.Random;

@SpringBootApplication
public class CommentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommentApplication.class, args);
	}

	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private UserRepository userRepository;
//	@Bean
//	CommandLineRunner commandLineRunner() {
//		return args -> {
//			Random random = new Random();
//			User user = new User("Pham", "Xuan","pcx",true,LocalDate.now());
//			userRepository.save(user);
//			for (int i = 0; i < 5; i++) {
//				Comment comment = new Comment("phim hay", LocalDate.now(), new Movie(1), user);
//				commentRepository.save(comment);
//			}
//		};
//	}
}
