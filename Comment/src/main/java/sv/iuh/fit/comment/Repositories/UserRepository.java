package sv.iuh.fit.comment.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sv.iuh.fit.comment.models.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
