package sv.iuh.fit.authjwt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import sv.iuh.fit.authjwt.authen.UserPrincipal;
import sv.iuh.fit.authjwt.entity.Profile;
import sv.iuh.fit.authjwt.entity.Token;
import sv.iuh.fit.authjwt.entity.User;
import sv.iuh.fit.authjwt.entity.UserInformation;
import sv.iuh.fit.authjwt.service.TokenService;
import sv.iuh.fit.authjwt.service.UserService;
import sv.iuh.fit.authjwt.util.JwtUtil;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService tokenService;

    @Value("${user.service.url}")
    private String userServiceUrl;
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserInformation userInformation) {
        User user = userInformation.getUser();
        Profile profile = userInformation.getProfile();
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(userServiceUrl + "createUser", profile, Profile.class);
        return ResponseEntity.ok(userService.createUser(user));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user, HttpSession session) {

        UserPrincipal userPrincipal =
                userService.findByUsername(user.getUsername());

        if (null == user || !new BCryptPasswordEncoder()
                .matches(user.getPassword(), userPrincipal.getPassword())) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Account or password is not valid!");
        }

        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));

        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);

        session.setAttribute("token", token.getToken());

        return ResponseEntity.ok(token.getToken());
    }


    @GetMapping("/hello")
//    @PreAuthorize("hasAnyAuthority('USER_READ')")
    public ResponseEntity hello() {
        return ResponseEntity.ok("hello");
    }


//    Object principal = SecurityContextHolder
//            .getContext().getAuthentication().getPrincipal();
//
//        if (principal instanceof UserDetails) {
//        UserPrincipal userPrincipal = (UserPrincipal) principal;
//    }

}
