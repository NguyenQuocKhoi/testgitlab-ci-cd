package sv.iuh.fit.authjwt.service;

import sv.iuh.fit.authjwt.authen.UserPrincipal;
import sv.iuh.fit.authjwt.entity.User;


public interface UserService {
    User createUser(User user);
    UserPrincipal findByUsername(String username);
}
