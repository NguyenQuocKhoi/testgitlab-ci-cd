package sv.iuh.fit.authjwt.service;

import sv.iuh.fit.authjwt.entity.Token;


public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);

}
