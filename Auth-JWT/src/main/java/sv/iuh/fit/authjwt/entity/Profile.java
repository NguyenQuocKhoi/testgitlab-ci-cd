package sv.iuh.fit.authjwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Profile {
    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private boolean gender;
    private LocalDate dob;
}
